import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  addNewResource,
  deleteResource,
  getResources,
  updateResourceData,
} from "../../api";
import { toast } from "react-toastify";

export const resourcesData = createAsyncThunk(
  "resourcesData",
  async (args, { rejectWithValue }) => {
    let response = await getResources();
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { resources: response };
    }
  }
);
export const addResource = createAsyncThunk(
  "addResource",
  async (args, { rejectWithValue }) => {
    let response = await addNewResource(args);
    if (response.error) {
      console.log("error----", response.error);
      toast.error(response.error);
      return rejectWithValue(response.error);
    } else {
      console.log("--inside toast---");
      toast.success("We've stored resource in our database.");
      return { newVendor: { response } };
    }
  }
);
export const removeResource = createAsyncThunk(
  "removeResource",
  async (resourceId, { rejectWithValue }) => {
    let response = await deleteResource(resourceId);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { deleteResponse: { response, resourceId } };
    }
  }
);

export const updateResource = createAsyncThunk(
  "updateResource",
  async (resourceData, { rejectWithValue }) => {
    let response = await updateResourceData(resourceData);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { updatedResource: response };
    }
  }
);

const resourceSlice = createSlice({
  name: "resource",
  initialState: {
    resources: [],
    isEditing: false,
    loading: true,
    error: null,
  },
  reducers: {
    setIsEditing: (state, action) => {
      state.isEditing = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(resourcesData.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(resourcesData.fulfilled, (state, action) => {
      state.loading = false;
      state.resources = action.payload.resources;
    });
    builder.addCase(resourcesData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(removeResource.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(removeResource.fulfilled, (state, action) => {
      state.loading = false;
      let filteredResources;

      if (action.payload.deleteResponse.response === 200) {
        let deletedId = action.payload.deleteResponse.resourceId;

        filteredResources = state.resources.filter((resource) => {
          if (resource._id === deletedId) return null;
          return resource;
        });
      }
      state.resources = filteredResources;
    });
    builder.addCase(removeResource.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(updateResource.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(updateResource.fulfilled, (state, action) => {
      state.loading = false;
      let updatedResources;

      updatedResources = state.resources.map((resource) => {
        if (resource._id === action.payload.updatedResource._id)
          return action.payload.updatedResource;
        return resource;
      });

      state.resources = updatedResources;
      state.isEditing = false;
    });
    builder.addCase(updateResource.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(addResource.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(addResource.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(addResource.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});
export default resourceSlice.reducer;
export const { setIsEditing } = resourceSlice.actions;
