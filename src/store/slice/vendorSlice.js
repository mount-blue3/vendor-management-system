import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  getVendors,
  deleteVendor,
  updateVendorsData,
  addNewVendor,
} from "../../api";
import { toast } from "react-toastify";


export const vendorsData = createAsyncThunk(
  "vendorsData",
  async (args, { rejectWithValue }) => {
    let response = await getVendors();
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { vendors: response };
    }
  }
);
export const removeVendor = createAsyncThunk(
  "removeVendor",
  async (vendorId, { rejectWithValue }) => {
    let response = await deleteVendor(vendorId);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { deleteResponse: { response, vendorId } };
    }
  }
);

export const addVendor = createAsyncThunk(
  "addVendor",
  async (args, { rejectWithValue }) => {
    let response = await addNewVendor(args);
    if (response.error) {
      toast.error(response.error);
      return rejectWithValue(response.error);
    } else {
      toast.success("We've stored vendor in our database.");
      return { newVendor: { response } };
    }
  }
);

export const updateVendor = createAsyncThunk(
  "updateVendor",
  async (vendorData, { rejectWithValue }) => {
    let response = await updateVendorsData(vendorData);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { updatedVendor: response };
    }
  }
);

const vendorSlice = createSlice({
  name: "vendor",
  initialState: {
    vendors: [],
    isEditing: false,
    loading: true,
    error: null,
  },
  reducers: {
    setIsEditing: (state, action) => {
      state.isEditing = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(vendorsData.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(vendorsData.fulfilled, (state, action) => {
      state.loading = false;
      state.vendors = action.payload.vendors;
    });
    builder.addCase(vendorsData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(removeVendor.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(removeVendor.fulfilled, (state, action) => {
      state.loading = false;
      let filteredVendors;

      if (action.payload.deleteResponse.response === 200) {
        let deletedId = action.payload.deleteResponse.vendorId;

        filteredVendors = state.vendors.filter((vendor) => {
          if (vendor._id === deletedId) return null;
          return vendor;
        });
      }
      state.vendors = filteredVendors;
    });
    builder.addCase(removeVendor.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(updateVendor.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(updateVendor.fulfilled, (state, action) => {
      state.loading = false;
      let updatedVendors;

      updatedVendors = state.vendors.map((vendor) => {
        if (vendor._id === action.payload.updatedVendor._id)
          return action.payload.updatedVendor;
        return vendor;
      });

      state.vendors = updatedVendors;
      state.isEditing = false;
    });
    builder.addCase(updateVendor.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(addVendor.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(addVendor.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(addVendor.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});
export default vendorSlice.reducer;
export const { setIsEditing } = vendorSlice.actions;
