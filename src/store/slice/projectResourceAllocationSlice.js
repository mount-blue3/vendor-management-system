import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  getProjectResourceData,
  newResourceToProject,
  unAssignResourceFromProject,
} from "../../api";
import { toast } from "react-toastify";

export const projectResourceAllocationData = createAsyncThunk(
  "projectResourceAllocationData",
  async (projectId, { rejectWithValue }) => {
    let response = await getProjectResourceData(projectId);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { projectResourceData: response };
    }
  }
);
export const addNewResourceToProject = createAsyncThunk(
  "addNewResourceToProject",
  async (args, { rejectWithValue }) => {
    let response = await newResourceToProject(args.data, args.id);
    if (response.error) {
      toast.error(response.error);
      return rejectWithValue(response.error);
    } else {
      toast.success(`We've added ${args.data.resource.name} to this project`);
      return { newResourceToProject: { response } };
    }
  }
);

export const unAssignResource = createAsyncThunk(
  "unAssignResource",
  async (args, { rejectWithValue }) => {
    let response = await unAssignResourceFromProject(args);
    if (response.error) {
      toast.error(response.error);
      return rejectWithValue(response.error);
    } else {
      toast.success(`Unassigned ${response.resource.name} from this project`);
      return { unAssignedResourceFromProjectResponse: response };
    }
  }
);

const projectResourceAllocationSlice = createSlice({
  name: "projectResourceAllocation",
  initialState: {
    projectResourceData: [],
    projectId: "",
    isEditing: false,
    selectedResource: {},
    isOpen: false,
    loading: true,
    error: null,
  },
  reducers: {
    setIsEditing: (state, action) => {
      state.isEditing = action.payload;
    },
    setProjectId: (state, action) => {
      state.projectId = action.payload;
    },
    setIsOpen: (state, action) => {
      state.isOpen = action.payload;
    },
    setSelectedResource: (state, action) => {
      state.selectedResource = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(projectResourceAllocationData.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(
      projectResourceAllocationData.fulfilled,
      (state, action) => {
        state.loading = false;
        state.projectResourceData = action.payload.projectResourceData;
      }
    );
    builder.addCase(projectResourceAllocationData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(addNewResourceToProject.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(addNewResourceToProject.fulfilled, (state, action) => {
      state.loading = false;
      state.projectResourceData.push(
        action.payload.newResourceToProject.response
      );
      state.isEditing = false;
    });
    builder.addCase(addNewResourceToProject.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(unAssignResource.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(unAssignResource.fulfilled, (state, action) => {
      state.loading = false;
      let updatedData;
      updatedData = state.projectResourceData.map((project) => {
        if (
          project._id ===
          action.payload.unAssignedResourceFromProjectResponse._id
        )
          return action.payload.unAssignedResourceFromProjectResponse;
        return project;
      });
      state.projectResourceData = updatedData;

      state.isEditing = false;
    });
    builder.addCase(unAssignResource.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});
export default projectResourceAllocationSlice.reducer;
export const { setIsEditing, setProjectId, setIsOpen, setSelectedResource } =
  projectResourceAllocationSlice.actions;
