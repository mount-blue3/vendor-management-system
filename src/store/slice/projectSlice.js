import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  getProjects,
  updateProjectData,
  deleteProject,
  addNewProject,
} from "../../api";
import { toast } from "react-toastify";

export const projectsData = createAsyncThunk(
  "projectsData",
  async (args, { rejectWithValue }) => {
    let response = await getProjects();
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { projects: response };
    }
  }
);
export const addProject = createAsyncThunk(
  "addProject",
  async (args, { rejectWithValue }) => {
    let response = await addNewProject(args);
    if (response.error) {
      console.log("error----", response.error);
      toast.error(response.error);
      return rejectWithValue(response.error);
    } else {
      console.log("--inside toast---");
      toast.success("We've stored project in our database.");
      return { newVendor: { response } };
    }
  }
);
export const removeProject = createAsyncThunk(
  "removeProject",
  async (projectId, { rejectWithValue }) => {
    let response = await deleteProject(projectId);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { deleteResponse: { response, projectId } };
    }
  }
);

export const updateProject = createAsyncThunk(
  "updateProject",
  async (projectData, { rejectWithValue }) => {
    let response = await updateProjectData(projectData);
    if (response.error) {
      return rejectWithValue(response.error);
    } else {
      return { updatedProject: response };
    }
  }
);

const projectSlice = createSlice({
  name: "project",
  initialState: {
    projects: [],
    isEditing: false,
    loading: true,
    error: null,
  },
  reducers: {
    setIsEditing: (state, action) => {
      state.isEditing = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(projectsData.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(projectsData.fulfilled, (state, action) => {
      state.loading = false;
      state.projects = action.payload.projects;
    });
    builder.addCase(projectsData.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(removeProject.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(removeProject.fulfilled, (state, action) => {
      state.loading = false;
      let filteredProjects;

      if (action.payload.deleteResponse.response === 200) {
        let deletedId = action.payload.deleteResponse.projectId;

        filteredProjects = state.projects.filter((project) => {
          if (project._id === deletedId) return null;
          return project;
        });
      }
      state.projects = filteredProjects;
    });
    builder.addCase(removeProject.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(updateProject.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(updateProject.fulfilled, (state, action) => {
      state.loading = false;
      let updatedProjects;

      updatedProjects = state.projects.map((project) => {
        if (project._id === action.payload.updatedProject._id)
          return action.payload.updatedProject;
        return project;
      });

      state.projects = updatedProjects;
      state.isEditing = false;
    });
    builder.addCase(updateProject.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(addProject.pending, (state) => {
      state.loading = false;
    });
    builder.addCase(addProject.fulfilled, (state, action) => {
      state.loading = false;
    });
    builder.addCase(addProject.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
  },
});
export default projectSlice.reducer;
export const { setIsEditing } = projectSlice.actions;
