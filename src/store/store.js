import { configureStore } from "@reduxjs/toolkit";
import vendorSlice from "./slice/vendorSlice";
import resourceSlice from "./slice/resourceSlice";
import projectSlice from "./slice/projectSlice";
import projectResourceSlice from "./slice/projectResourceAllocationSlice";

const store = configureStore({
  reducer: {
    vendor: vendorSlice,
    resource: resourceSlice,
    project: projectSlice,
    projectResourceAllocation: projectResourceSlice,
  },
});
export default store;
