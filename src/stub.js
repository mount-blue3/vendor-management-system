export const stubData = [
  {
    _id: "123",
    resource: {
      id: "345",
      resource_name: "Nakkul",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "120",
    resource: {
      id: "340",
      resource_name: "anusha",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "121",
    resource: {
      id: "341",
      resource_name: "vishal",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "124",
    resource: {
      id: "342",
      resource_name: "Ashiq",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "125",
    resource: {
      id: "343",
      resource_name: "Anu",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "126",
    resource: {
      id: "344",
      resource_name: "Arun",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "127",
    resource: {
      id: "346",
      resource_name: "Satya",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "128",
    resource: {
      id: "347",
      resource_name: "Yaswanth",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
  {
    _id: "129",
    resource: {
      id: "348",
      resource_name: "Priyank",
    },
    vendor: {
      id: "546",
      name: "Amazon",
    },
    start_date: "12/12/23",
    end_date: null,
    project: {
      id: "465",
      name: "DC web",
    },
  },
];
