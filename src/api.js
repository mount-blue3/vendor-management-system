import axios from "axios";

export const getVendors = async () => {
  try {
    const response = await axios.get("http://localhost:5000/vendors");
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const getVendor = async (slug) => {
  try {
    const response = await axios.get(
      `http://localhost:5000/vendors/single-vendor?slug=${slug}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const addNewVendor = async (data) => {
  try {
    const response = await axios.post("http://localhost:5000/vendors", data);
    if (response) console.log("created vendor successfully", response);
    return response.data;
  } catch (error) {
    console.log("error in api", error.response.data);
    if (error.response.data.error.message)
      return { error: error.response.data.error.message };
    else return { error: error.response.data };
  }
};
export const updateVendorsData = async (updatedData) => {
  try {
    const response = await axios.put(
      "http://localhost:5000/vendors",
      updatedData
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};

export const deleteVendor = async (vendorId) => {
  try {
    const response = await axios.delete(
      `http://localhost:5000/vendors?id=${vendorId}`
    );
    return response.status;
  } catch (error) {
    console.log(error);
  }
};

export const getResources = async () => {
  try {
    const response = await axios.get("http://localhost:5000/resources");
    return response.data;
  } catch (error) {
    console.log(error);
  }
};
export const addNewResource = async (data) => {
  try {
    const response = await axios.post("http://localhost:5000/resources", data);
    return response.data;
  } catch (error) {
    if (error.response.data.error.message)
      return { error: error.response.data.error.message };
    else return { error: "You can try again, make sure email is unique." };
  }
};
export const updateResourceData = async (updatedData) => {
  try {
    const response = await axios.put(
      "http://localhost:5000/resources",
      updatedData
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};

export const deleteResource = async (ResourceId) => {
  try {
    const response = await axios.delete(
      `http://localhost:5000/resources?id=${ResourceId}`
    );
    return response.status;
  } catch (error) {
    console.log(error);
  }
};

export const getProjects = async () => {
  try {
    const response = await axios.get("http://localhost:5000/projects");
    return response.data;
  } catch (error) {
    console.log(error);
  }
};
export const addNewProject = async (data) => {
  try {
    const response = await axios.post("http://localhost:5000/projects", data);
    return response.data;
  } catch (error) {
    return { error: error.response.data.error.message };
  }
};
export const updateProjectData = async (updatedData) => {
  try {
    const response = await axios.put(
      "http://localhost:5000/projects",
      updatedData
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return { error: error.message };
  }
};

export const deleteProject = async (projectId) => {
  try {
    const response = await axios.delete(
      `http://localhost:5000/projects?id=${projectId}`
    );
    return response.status;
  } catch (error) {
    console.log(error);
  }
};

export const getProjectResourceData = async (projectId) => {
  try {
    const response = await axios.get(
      `http://localhost:5000/resourceAllocation/project/staff?project_id=${projectId}`
    );
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const newResourceToProject = async (data, projectId) => {
  try {
    const response = await axios.post(
      `http://localhost:5000/resourceAllocation/project/assign?project_id=${projectId}`,
      data
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return { error: error.response.data.error.message };
  }
};

export const unAssignResourceFromProject = async (data) => {
  try {
    const response = await axios.post(
      `http://localhost:5000/resourceAllocation/project/unassign`,
      data
    );
    return response.data;
  } catch (error) {
    console.log(error);
    return { error: error.response.data.error.message };
  }
};

export const getUniqueResource = async (resourceId) => {
  try {
    const response = await axios.get(`http://localhost:5000/resources/resource?id=${resourceId}`);
    console.log("response", response.data);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const getUniqueVendor = async (vendorId) => {
  try {
    const response = await axios.get(`http://localhost:5000/vendors/vendor?id=${vendorId}`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};