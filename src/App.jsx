import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "./App.css";
import Home from "./components/Home";
import VendorRegistrationForm from "./components/VendorComponents/VendorRegestrationForm";
import DisplayOptionsPage from "./components/DisplayOptionsPage";
import VendorsData from "./components/VendorComponents//VendorsData";
import ResourceRegistrationForm from "./components/ResourceComponents/ResourceRegistrationForm";
import ResourcesData from "./components/ResourceComponents/ResourcesData";
import ProjectsForm from "./components/ProjectComponents/ProjectsForm";
import ProjectsData from "./components/ProjectComponents/ProjectsData";
import SingleVendor from "./components/SingleVendor";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ProjectResourceAllocationPage from "./components/ProjectResourceAllocation/ProjectResourceAllocationPage";
import ProjectResourceAllocationForm from "./components/ProjectResourceAllocation/ProjectResourceAllocationForm";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/options" element={<DisplayOptionsPage />} />
          <Route path="/allVendors" element={<VendorsData />} />
          <Route path="/allVendors/:vendorName" element={<SingleVendor />} />
          <Route path="/allResources" element={<ResourcesData />} />
          <Route path="/newVendor" element={<VendorRegistrationForm />} />
          <Route path="/newResource" element={<ResourceRegistrationForm />} />
          <Route path="/newProject" element={<ProjectsForm />} />
          <Route path="/allProjects">
            <Route exact path="/allProjects" element={<ProjectsData />} />
            <Route
              path="/allProjects/:id/staff"
              element={<ProjectResourceAllocationPage />}
            />
          </Route>
        </Routes>
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
