import React, { useState } from "react";
import { Box, Text, Flex, Input, Button } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { EditIcon } from "@chakra-ui/icons";
import { getVendor, updateVendorsData } from "../api";

const SingleVendor = () => {
  let slug = useParams();
  const [vendor, setVendor] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const { register, setValue, handleSubmit, reset } = useForm({
    defaultValues: {
      vendorName: vendor?.name,
      shortName: vendor?.short_name,
      contactPersonName: vendor?.person_of_contact_name,
      contactPersonEmail: vendor?.person_of_contact_email,
      contactPersonMobileNumber: vendor?.person_of_contact_mobile,
    },
  });

  useEffect(() => {
    const fetchSingleVendor = async () => {
      let response = await getVendor(slug.vendorName);
      if (response.error) {
        console.log(error);
      } else {
        console.log("single vendor", response);
        setVendor(response);
        setValue("vendorName", response?.name || "");
        setValue("shortName", response?.short_name || "");
        setValue("contactPersonName", response?.person_of_contact_name || "");
        setValue("contactPersonEmail", response?.person_of_contact_email || "");
        setValue(
          "contactPersonMobileNumber",
          response?.person_of_contact_mobile || ""
        );
      }
    };
    fetchSingleVendor();
  }, []);

  const onSubmit = async (data) => {
    //setIsEditing(false);
    console.log("---vendor._id--", data.vendorName);
    let response = await updateVendorsData({
      id: vendor._id,
      name: data.vendorName,
      person_of_contact_name: data.contactPersonName,
      person_of_contact_email: data.contactPersonEmail,
      person_of_contact_mobile: data.contactPersonMobileNumber,
      short_name: data.shortName,
    });
    console.log("update response---", response);

    if (response.error) {
      console.log(error);
    } else {
      setVendor(response.data);
    }
  };

  const handleEditClick = (e) => {
    setIsEditing(true);
    e.stopPropagation();

    setValue("vendorName", vendor.name);
    setValue("shortName", vendor.short_name);
    setValue("contactPersonName", vendor.person_of_contact_name);
    setValue("contactPersonEmail", vendor.person_of_contact_email);
    setValue("contactPersonMobileNumber", vendor.person_of_contact_mobile);
  };

  const handleCancelChanges = () => {
    setIsEditing(false);
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Flex flexDirection="column" rowGap="1rem" p="10">
          <Text fontWeight="bold">Vendor Name:</Text>
          <Input
            {...register("vendorName")}
            readOnly={!isEditing}
            color="black"
          ></Input>

          <Text fontWeight="bold">Short Name:</Text>
          <Input {...register("shortName")} readOnly={!isEditing}></Input>

          <Text fontWeight="bold">Contact Person Name:</Text>
          <Input
            {...register("contactPersonName")}
            readOnly={!isEditing}
          ></Input>

          <Text fontWeight="bold">Contact Person Email:</Text>
          <Input
            {...register("contactPersonEmail")}
            readOnly={!isEditing}
          ></Input>

          <Text fontWeight="bold">Contact Person Number:</Text>
          <Input
            {...register("contactPersonMobileNumber")}
            readOnly={!isEditing}
          ></Input>

          <Box display="flex" justifyContent="end">
            {isEditing ? (
              <>
                <Button colorScheme="blue" mr={3} type="submit">
                  Save Changes
                </Button>
                <Button colorScheme="blue" mr={3} onClick={handleCancelChanges}>
                  Cancel
                </Button>
              </>
            ) : (
              <Button
                colorScheme="blue"
                size="lg"
                leftIcon={<EditIcon />}
                mr={3}
                onClick={handleEditClick}
              >
                Edit
              </Button>
            )}
          </Box>
        </Flex>
      </form>
    </>
  );
};

export default SingleVendor;
