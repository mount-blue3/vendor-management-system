import React from "react";
import { Box, Heading, Flex } from "@chakra-ui/react";
import { Image } from "@chakra-ui/react";

function Header() {
  return (
    <header>
      <Box p="5" boxShadow="0 1px 3px rgba(0, 0, 0, 0.3)">
        <Flex alignItems="center">
          <Image
            boxSize="50px"
            width="100px"
            borderRadius="10"
            src="avisLogo.jpg"
            objectFit="cover"
            alt="AVIS"
          />
          <Box ml="5">
            <Flex columnGap="0.5rem">
              <Heading color="#eb3c14" fontSize="2rem">
                Avis Budget Group
              </Heading>
            </Flex>
          </Box>
        </Flex>
      </Box>
    </header>
  );
}

export default Header;
