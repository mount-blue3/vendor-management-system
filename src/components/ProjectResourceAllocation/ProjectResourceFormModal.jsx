import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import ProjectResourceAllocationForm from "./ProjectResourceAllocationForm";

const ProjectResourceFormModal = ({ isOpen, onClose }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Project Resource Allocation Form</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <ProjectResourceAllocationForm />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
export default ProjectResourceFormModal;
