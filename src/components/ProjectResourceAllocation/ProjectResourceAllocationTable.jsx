import React, { useEffect } from "react";
import { Table, Thead, Tbody, Tr, Th, TableContainer } from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { projectResourceAllocationData } from "../../store/slice/projectResourceAllocationSlice";
import { resourcesData } from "../../store/slice/resourceSlice";
import ResourceAllocationTableRow from "./ResourceAllocationTableRow";
import AlertDialogueForUnAssign from "./AlertDialogueForUnAssign";

const ProjectResourceAllocationTable = () => {
  const reduxDispatch = useDispatch();
  let projectId = useSelector(
    (state) => state.projectResourceAllocation.projectId
  );
  let projectResourceData = useSelector(
    (state) => state.projectResourceAllocation.projectResourceData
  );
  let open = useSelector((state) => state.projectResourceAllocation.isOpen);

  useEffect(() => {
    reduxDispatch(projectResourceAllocationData(projectId));
    reduxDispatch(resourcesData());
  }, []);

  return (
    <div>
      {open && <AlertDialogueForUnAssign />}
      <TableContainer>
        <Table variant="striped" colorScheme="gray">
          <Thead>
            <Tr>
              <Th>Resource</Th>
              <Th>Start date</Th>
              <Th>Vendor</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
          <Tbody>
            {projectResourceData?.map((item) => {
              return <ResourceAllocationTableRow key={item._id} item={item} />;
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default ProjectResourceAllocationTable;
