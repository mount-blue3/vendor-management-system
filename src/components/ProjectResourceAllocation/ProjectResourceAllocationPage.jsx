import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Button } from "@chakra-ui/react";
import { useDispatch } from "react-redux";
import Header from "../Header";
import ProjectResourceAllocationTable from "./ProjectResourceAllocationTable";
import ProjectResourceFormModal from "./ProjectResourceFormModal.jsx";
import { setProjectId } from "../../store/slice/projectResourceAllocationSlice";

const ProjectResourceAllocationPage = () => {
  const projectId = useParams().id;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const reduxDispatch = useDispatch();
  reduxDispatch(setProjectId(projectId));

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Header />
      <Button m="7" colorScheme="blue" onClick={openModal}>
        Add Resource
      </Button>
      <ProjectResourceFormModal isOpen={isModalOpen} onClose={closeModal} />
      <ProjectResourceAllocationTable projectId={projectId} />
    </>
  );
};

export default ProjectResourceAllocationPage;
