import React,{useState} from "react";
import { useDispatch } from "react-redux";
import {
  setIsOpen,
  setSelectedResource,
} from "../../store/slice/projectResourceAllocationSlice";
import { Tr, Td, Button, useDisclosure } from "@chakra-ui/react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AlertDialogueForUnAssign from "./AlertDialogueForUnAssign";
import { getUniqueResource, getUniqueVendor } from "../../api";
import ResourceModal from "../ResourceComponents/ResourceModal";
import VendorModal from "../VendorComponents/VendorModal";

const ResourceAllocationTableRow = ({ item }) => {
  const resourceDisclosure = useDisclosure();
  const vendorDisclosure = useDisclosure();
  const reduxDispatch = useDispatch();
  const [resource, setResource] = useState("");
  const [vendor, setVendor] = useState("");
 
  const handleUnassignButton = (item) => {
    reduxDispatch(setIsOpen(true));
    reduxDispatch(setSelectedResource(item));
  };

  const showToast = () => {
    toast.error("Data not available!", {
      position: toast.POSITION.TOP_RIGHT,
    });
  };

  const handleDisplayResource = async () => {
    try {
      console.log("resource id",item.resource.id);
      const response = await getUniqueResource(item.resource.id);
      setResource(response);
      response ? resourceDisclosure.onOpen() : showToast();
    } catch (error) {
      console.log(error);
    }
  };

  const handleDisplayVendor = async () => {
    try {
      const response = await getUniqueVendor(item.vendor.id);
      setVendor(response);
      response ? vendorDisclosure.onOpen() : showToast();
    } catch (error) {
      console.log(error);
    }
  };
  // console.log(item)

  return (
    <>
      <Tr>
        <Td>
          <Button onClick={handleDisplayResource} variant="ghost">
            {item.resource.name}
          </Button>
        </Td>
        <Td>{item.start_date}</Td>
        <Td>
          <Button onClick={handleDisplayVendor} variant="ghost">
            {item.vendor.name}
          </Button>
        </Td>
        <Td>
          {item.end_date ? (
            <span style={{ color: "red" }}>Unassigned</span>
          ) : (
            <Button variant="ghost" onClick={() => handleUnassignButton(item)}>
              Unassign
            </Button>
          )}
        </Td>
      </Tr>
      {resourceDisclosure.isOpen && (
        <ResourceModal
          open={resourceDisclosure.isOpen}
          close={resourceDisclosure.onClose}
          resource={resource}
        />
      )}
      {vendorDisclosure.isOpen && (
        <VendorModal
          open={vendorDisclosure.isOpen}
          close={vendorDisclosure.onClose}
          vendor={vendor}
          blockHistoryBack={true}
        />
      )}
    </>
  );
};

export default ResourceAllocationTableRow;
