import {
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from "@chakra-ui/react";
import { useSelector, useDispatch } from "react-redux";
import {
  setIsOpen,
  unAssignResource,
} from "../../store/slice/projectResourceAllocationSlice";

function AlertDialogueForUnAssign() {
  const reduxDispatch = useDispatch();
  const open = useSelector((state) => state.projectResourceAllocation.isOpen);
  const resource = useSelector(
    (state) => state.projectResourceAllocation.selectedResource
  );

  const handleCancel = () => {
    reduxDispatch(setIsOpen(false));
  };
  const formatDate = () => {
    const formattedDate = new Date();
    const year = formattedDate.getFullYear();
    const month = String(formattedDate.getMonth() + 1).padStart(2, "0");
    const day = String(formattedDate.getDate()).padStart(2, "0");

    return `${year}-${month}-${day}`;
  };
  const handleUnAssign = () => {
    let date = formatDate();
    reduxDispatch(
      unAssignResource({
        resource_allocation_id: resource._id,
        end_date: date,
      })
    ).then((res) => {
      if (res.type === "unAssignResource/fulfilled") {
        handleCancel();
      }
    });
  };

  return (
    <>
      <AlertDialog isOpen={open} onClose={!open}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">UnAssign the resource</AlertDialogHeader>
            <AlertDialogBody>
               Are you sure you want to unassign this resource?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button onClick={handleCancel}>Cancel</Button>
              <Button colorScheme="red" onClick={handleUnAssign} ml={3}>
                UnAssign
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
}
export default AlertDialogueForUnAssign;
