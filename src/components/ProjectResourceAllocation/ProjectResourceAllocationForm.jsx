import {
  FormLabel,
  MenuButton,
  MenuItem,
  Input,
  Menu,
  Text,
  MenuList,
  Button,
  FormControl,
  Box,
  Heading,
} from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { resourcesData } from "../../store/slice/resourceSlice";
import { useDispatch, useSelector } from "react-redux";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { addNewResourceToProject } from "../../store/slice/projectResourceAllocationSlice";

function ProjectResourceAllocationForm() {
  const [selectedResource, setSelectedResource] = useState(null);
  const [selectedDate, setSelectedDate] = useState(null);
  const reduxDispatch = useDispatch();
  let resourcesDataToSelect = useSelector((state) => state.resource.resources);
  const projectId = useSelector(
    (state) => state.projectResourceAllocation.projectId
  );
  console.log("project id in form", projectId);

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      resource: {},
      startDate: "",
    },
  });

  useEffect(() => {
    reduxDispatch(resourcesData());
  }, []);

  const formatDate = (date) => {
    if (!date) {
      return "";
    }

    const formattedDate = new Date(date);
    const year = formattedDate.getFullYear();
    const month = String(formattedDate.getMonth() + 1).padStart(2, "0");
    const day = String(formattedDate.getDate()).padStart(2, "0");

    return `${year}-${month}-${day}`;
  };
  const handleDateChange = (date) => {
    setSelectedDate(date);
    console.log(formatDate(date));
    setValue("startDate", formatDate(date));
  };
  const handleResourceSelection = (resource) => {
    setSelectedResource({
      id: resource._id,
      name: resource.first_name + " " + resource.last_name,
    });
    setValue("resource", {
      id: resource._id,
      name: resource.first_name + " " + resource.last_name,
    });
  };
  const onSubmit = async (data) => {
    console.log("form submitted", data, projectId);
    const newResourceInProject = {
      resource: data.resource,
      start_date: data.startDate,
    };
    reduxDispatch(
      addNewResourceToProject({ data: newResourceInProject, id: projectId })
    );
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl padding="2rem">
          <FormLabel>
            Resources <span style={{ color: "red" }}>*</span>
          </FormLabel>
          <Menu {...register("resource")}>
            <MenuButton as={Button} rightIcon={<ChevronDownIcon />} isRequired>
              {selectedResource ? selectedResource.name : "Resources"}
            </MenuButton>
            <MenuList>
              {resourcesDataToSelect.map((item) => {
                return (
                  <MenuItem onClick={() => handleResourceSelection(item)}>
                    {item.first_name + " " + item.last_name}
                  </MenuItem>
                );
              })}
            </MenuList>
          </Menu>
          {selectedResource?.id === undefined && (
            <Text mt={2} color="red">
              This field is required
            </Text>
          )}
          <FormLabel mt="1rem">
            From (startDate) <span style={{ color: "red" }}>*</span>
          </FormLabel>
          <DatePicker
            {...register("startDate", {
              required: "This field is required",
            })}
            selected={selectedDate}
            onChange={handleDateChange}
            dateFormat="dd/MM/yyyy"
            customInput={<Input />}
            placeholderText="Select a date"
          />
          <Text mt={2} color="red">
            {errors.startDate?.message}
          </Text>
        </FormControl>
        <Box display="flex" justifyContent="center" pt="1rem">
          <Button
            type="submit"
            width="20rem"
            bgColor="blue"
            mb="2rem"
            color="white"
          >
            Submit
          </Button>
        </Box>
      </form>
    </>
  );
}
export default ProjectResourceAllocationForm;
