import React, { useEffect } from "react";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Text,
  Flex,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Textarea,
} from "@chakra-ui/react";
import { EditIcon, ChevronDownIcon } from "@chakra-ui/icons";
import { useForm } from "react-hook-form";
import { resourcesData } from "../../store/slice/resourceSlice";
import { updateProject, setIsEditing } from "../../store/slice/projectSlice";
import { useDispatch, useSelector } from "react-redux";

const ProjectsModal = (props) => {
  const schema = yup.object().shape({
    dev_url: yup.string().url("Invalid Dev URL format"),
    qa_url: yup.string().url("Invalid QA URL format"),
    uat_url: yup.string().url("Invalid UAT URL format"),
    live_url: yup.string().url("Invalid Live URL format"),
  });
  const project = props.project;
  const reduxDispatch = useDispatch();
  let isEditing = useSelector((state) => {
    console.log("projects data isEditing", state.project.isEditing);
    return state.project.isEditing;
  });
  let resources = useSelector((state) => {
    console.log("resources data", state.resource.resources);
    return state.resource.resources;
  });

  const {
    register,
    setValue,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      projectName: project.name,
      shortName: project.short_name,
      description: project.description,
      engineeringManager: project.engineering_manager,
      dev_url: project.dev_url,
      qa_url: project.qa_url,
      uat_url: project.uat_url,
      live_url: project.live_url,
    },
    resolver: yupResolver(schema),
  });
  const engineeringManager = watch("engineeringManager");
  const projectName = watch("projectName");

  useEffect(() => {
    reduxDispatch(resourcesData());
  }, []);

  const { ref } = register("projectName");

  const handleOnBlur = () => {
    console.log("calling on blur");
    const generateShortName = () => {
      let shortName = [];
      projectName
        .split(" ")
        .map((item) => shortName.push(item[0]?.toUpperCase()));
      setValue("shortName", shortName.join(""));
    };
    if (projectName) {
      generateShortName();
    }
  };

  const handleEditClick = () => {
    reduxDispatch(setIsEditing(true));
    setValue("projectName", project.name);
    setValue("shortName", project.short_name);
    setValue("description", project.description);
    setValue("engineeringManager", project.engineering_manager);
    setValue("dev_url", project.dev_url);
    setValue("qa_url", project.qa_url);
    setValue("uat_url", project.uat_url);
    setValue("live_url", project.live_url);
  };

  const onSubmit = async (data) => {
    let updatedProjectObject = {
      id: project._id,
      name: data.projectName,
      short_name: data.shortName,
      description: data.description,
      engineering_manager: data.engineeringManager,
      dev_url: data.dev_url,
      qa_url: data.qa_url,
      uat_url: data.uat_url,
      live_url: data.live_url,
    };

    reduxDispatch(updateProject(updatedProjectObject));
  };

  const handleCancelChanges = () => {
    reduxDispatch(setIsEditing(false));
  };

  const handleClose = () => {
    reduxDispatch(setIsEditing(false));
    props.close();
  };

  return (
    <>
      <Modal isOpen={props.open} onClose={handleClose} size="xl">
        <ModalOverlay />

        <ModalContent>
          <ModalHeader>Project Information</ModalHeader>
          <ModalCloseButton />

          <ModalBody>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Flex flexDirection="column" rowGap="1rem">
                <Flex columnGap={1}>
                  <Text fontWeight="bold">Project Name</Text>
                  <Text color="red">*</Text>
                </Flex>
                <Input
                  type="text"
                  {...register("projectName", {
                    required: "This field is required",
                  })}
                  readOnly={!isEditing}
                  onBlur={handleOnBlur}
                  ref={ref}
                ></Input>
                <Text mt={2} color="red">
                  {errors.projectName?.message}
                </Text>
                <Flex columnGap={1}>
                  <Text fontWeight="bold">Short Name</Text>
                  <Text color="red">*</Text>
                </Flex>
                <Input
                  type="text"
                  {...register("shortName", {
                    required: "This field is required",
                  })}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.shortName?.message}
                </Text>
                <Text fontWeight="bold">Description:</Text>
                <Textarea {...register("description")} readOnly={!isEditing} />
                <Text fontWeight="bold">Engineering Manager:</Text>
                <Menu {...register("engineeringManager")}>
                  <MenuButton
                    as={Button}
                    isDisabled={!isEditing}
                    rightIcon={<ChevronDownIcon />}
                  >
                    {engineeringManager.resource
                      ? engineeringManager.resource
                      : "Resources"}
                  </MenuButton>
                  <MenuList>
                    <MenuItem
                      onClick={() => setValue("engineeringManager", {})}
                    >
                      Select
                    </MenuItem>
                    {resources?.map((item, index) => (
                      <MenuItem
                        key={index}
                        onClick={() =>
                          setValue("engineeringManager", {
                            resource_id: item._id,
                            resource: item.first_name + " " + item.last_name,
                          })
                        }
                      >
                        {item?.first_name + " " + item?.last_name}
                      </MenuItem>
                    ))}
                  </MenuList>
                </Menu>
                <Text mt={2} color="red">
                  {errors.engineeringManager?.message}
                </Text>
                <Text fontWeight="bold">Dev URL</Text>
                <Input {...register("dev_url")} readOnly={!isEditing}></Input>
                <Text mt={2} color="red">
                  {errors.dev_url?.message}
                </Text>
                <Text fontWeight="bold">QA URL:</Text>
                <Input {...register("qa_url")} readOnly={!isEditing}></Input>
                <Text mt={2} color="red">
                  {errors.qa_url?.message}
                </Text>
                <Text fontWeight="bold">UAT URL:</Text>
                <Input {...register("uat_url")} readOnly={!isEditing}></Input>
                <Text mt={2} color="red">
                  {errors.uat_url?.message}
                </Text>
                <Text fontWeight="bold">Live URL:</Text>
                <Input {...register("live_url")} readOnly={!isEditing}></Input>
                <Text mt={2} color="red">
                  {errors.live_url?.message}
                </Text>
                <Box display="flex" justifyContent="end">
                  {isEditing && (
                    <>
                      <Button colorScheme="blue" mr={3} type="submit">
                        Save Changes
                      </Button>
                      <Button
                        colorScheme="blue"
                        mr={3}
                        onClick={handleCancelChanges}
                      >
                        Cancel
                      </Button>
                    </>
                  )}
                </Box>
              </Flex>
            </form>
            {!isEditing && (
              <Box>
                <Button
                  colorScheme="blue"
                  size="lg"
                  leftIcon={<EditIcon />}
                  mr={3}
                  onClick={handleEditClick}
                >
                  Edit
                </Button>
              </Box>
            )}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ProjectsModal;
