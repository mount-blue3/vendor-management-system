import React from "react";
import { Link } from "react-router-dom";
import {
  Card,
  Heading,
  Flex,
  useDisclosure,
  Button,
  Box,
  Container,
} from "@chakra-ui/react";
import { FaUser, FaPlus } from "react-icons/fa";
import { DeleteIcon } from "@chakra-ui/icons";
import ProjectModal from "./ProjectsModal";
import DeleteConfirmation from "../DeleteConfirmation";


const ProjectCard = (props) => {
  let project = props.resourceData;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isDialogOpen,
    onOpen: onDialogOpen,
    onClose: onDialogClose,
  } = useDisclosure();

  const handleClick = () => {
    onOpen();
  };

  const handleDelete = (e) => {
    props.delete(e, project._id);
  };

  return (
    <>
      <Card
        bgColor="#FA9884"
        color="white"
        width="20rem"
        height="4rem"
        boxShadow="md"
        cursor={"pointer"}
        onClick={() => handleClick()}
        m="2.5rem"
        p="2"
      >
        <Flex justifyContent="space-between" alignItems="center" width="130%">
          <Container>
            <Heading size="md">{project.name}</Heading>
          </Container>
          <Container>
            <Link
              to={`/allProjects/${project._id}/staff`}
              key={project._id}
            >
              <Button bgColor="#FA9884">
                <Box>
                  <FaUser size={16} />
                  <FaPlus size={12} />
                </Box>
              </Button>
            </Link>
            <Button
              bgColor="#FA9884"
              onClick={(e) => {
                e.stopPropagation();
                onDialogOpen();
              }}
            >
              <DeleteIcon color="red" />
            </Button>
            {/* <DeleteIcon
              ml="3"
              color="red"
              cursor="pointer"
              onClick={(e) => {
                e.stopPropagation();
                onDialogOpen();
              }}
            /> */}
          </Container>
        </Flex>
      </Card>
      <DeleteConfirmation
        handleDelete={handleDelete}
        category="Project"
        isOpen={isDialogOpen}
        onClose={onDialogClose}
      />
      {isOpen && (
        <ProjectModal open={isOpen} close={onClose} project={project} />
      )}
    </>
  );
};

export default ProjectCard;
