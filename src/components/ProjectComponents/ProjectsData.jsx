import React, { useEffect } from "react";
import { Box, Heading, Flex, Button } from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";
import Header from "../Header";
import { Link } from "react-router-dom";
import ProjectCard from "./ProjectsCard";
import { projectsData, removeProject } from "../../store/slice/projectSlice";
import { useDispatch, useSelector } from "react-redux";

const ProjectsData = () => {
  const reduxDispatch = useDispatch();
  let projects = useSelector((state) => {
    console.log("projects data", state.project.projects);
    return state.project.projects;
  });

  useEffect(() => {
    reduxDispatch(projectsData());
  }, []);

  const handleDeleteProject = async (event, id) => {
    event.stopPropagation();
    reduxDispatch(removeProject(id));
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box height="auto">
        <Flex flexDirection="column" alignItems="center">
          <Heading color="black">All Projects</Heading>
          <Flex flexWrap="wrap" justifyContent="center">
            {projects?.map((item) => (
              <ProjectCard
                key={item._id}
                resourceData={item}
                delete={handleDeleteProject}
              />
            ))}
          </Flex>
        </Flex>
      </Box>
    </>
  );
};

export default ProjectsData;
