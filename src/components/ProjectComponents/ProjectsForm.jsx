import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Button,
  Input,
  Text,
  Card,
  Heading,
  Box,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Textarea,
  Flex,
} from "@chakra-ui/react";
import Header from "../Header";
import { ArrowBackIcon, ChevronDownIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addProject } from "../../store/slice/projectSlice";
import { resourcesData } from "../../store/slice/resourceSlice";

function ProjectsForm() {
  // const schema = yup.object().shape({
  //   projectName: yup.string().required("This field is required"),
  //   shortName: yup.string().required("This field is required"),
  //   description: yup.string(),
  //   engineeringManager: yup
  //     .mixed()
  //     .test(
  //       "required",
  //       "Select an engineering manager",
  //       (value) => value.resource_id
  //     ),
  //   dev_url: yup.string().url("Invalid Dev URL format"),
  //   qa_url: yup.string().url("Invalid QA URL format"),
  //   uat_url: yup.string().url("Invalid UAT URL format"),
  //   live_url: yup.string().url("Invalid Live URL format"),
  // });

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      projectName: "",
      shortName: "",
      description: "",
      engineeringManager: {},
      dev_url: "",
      qa_url: "",
      uat_url: "",
      live_url: "",
    },
    // resolver: yupResolver(schema),
  });

  const projectName = watch("projectName");
  const engineeringManager = watch("engineeringManager");
  const [selectedEngineeringManager, setSelectedEngineeringManager] = useState(
    {}
  );
  const [isSubmitted, setIsSubmitted] = useState(false);

  const reduxDispatch = useDispatch();
  let resources = useSelector((state) => {
    return state.resource.resources;
  });

  useEffect(() => {
    reduxDispatch(resourcesData());
  }, []);

  const { ref } = register("projectName");

  const handleOnBlur = () => {
    const generateShortName = () => {
      let shortName = [];
      projectName
        .split(" ")
        .map((item) => shortName.push(item[0]?.toUpperCase()));
      setValue("shortName", shortName.join(""));
    };
    if (projectName) {
      generateShortName();
    }
  };
  const validateMenu = (value) => {
    return value !== "" || "Please select an option";
  };

  const onSubmit = async (data) => {
    const newProjectObject = {
      name: data.projectName,
      short_name: data.shortName,
      description: data.description,
      engineering_manager: data.engineeringManager,
      dev_url: data.dev_url,
      qa_url: data.qa_url,
      uat_url: data.uat_url,
      live_url: data.live_url,
    };

    reduxDispatch(addProject(newProjectObject));
    reset();
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box
        width="100%"
        height="100vh"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Heading color="black" my="5">
          Create Project
        </Heading>
        <Card width="50%" boxShadow="2px 2px 4px 2px rgba(0, 0, 0, 0.2)">
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormControl padding="2rem" isInvalid={errors.engineeringManager}>
              <FormLabel>
                Name of the Project <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("projectName", {
                  required: "This field is required",
                })}
                ref={ref}
                onBlur={handleOnBlur}
              />
              <Text mt={2} color="red">
                {errors.projectName?.message}
              </Text>
              <Flex columnGap={1}>
                <FormLabel>Short Name</FormLabel>
                <Text color="red">*</Text>
              </Flex>
              <Input
                type="text"
                {...register("shortName", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.shortName?.message}
              </Text>
              <FormLabel>Description</FormLabel>
              <Textarea {...register("description")} />
              <Flex columnGap={1} alignItems="center">
                <FormLabel mt="1rem">Engineering Manager</FormLabel>
                <Text color="red">*</Text>
              </Flex>
              <Menu closeOnSelect={true}>
                <MenuButton
                  as={Button}
                  rightIcon={<ChevronDownIcon />}
                  mb="1rem"
                >
                  {engineeringManager.resource_id !== undefined
                    ? engineeringManager.resource
                    : "Resources"}
                </MenuButton>
                <MenuList>
                  <MenuItem>Select</MenuItem>
                  {resources?.map((item, index) => (
                    <MenuItem
                      key={index}
                      {...register("engineeringManager", {
                        validate: validateMenu,
                      })}
                      onClick={() =>
                        setValue("engineeringManager", {
                          resource_id: item._id,
                          resource: item.first_name + item.last_name,
                        })
                      }
                    >
                      {`${item?.first_name} ${item?.last_name}`}
                    </MenuItem>
                  ))}
                </MenuList>
              </Menu>

              {errors.engineeringManager && (
                <FormErrorMessage>
                  {errors.engineeringManager.message}
                </FormErrorMessage>
              )}

              <FormLabel>Dev_URL</FormLabel>
              <Input type="text" {...register("dev_url")} />
              <FormLabel mt="1rem">QA_URL</FormLabel>
              <Input type="text" {...register("qa_url")} />
              <FormLabel mt="1rem">UAT_URL</FormLabel>
              <Input type="text" {...register("uat_url")} />
              <FormLabel mt="1rem">Live_URL</FormLabel>
              <Input type="text" {...register("live_url")} />
            </FormControl>

            <Box display="flex" justifyContent="center">
              <Button
                type="submit"
                width="20rem"
                color="white"
                mb="2rem"
                bgColor="blue"
              >
                Submit
              </Button>
            </Box>
          </form>
        </Card>
      </Box>
    </>
  );
}

export default ProjectsForm;
