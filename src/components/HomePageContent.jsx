import React from "react";
import { Box, Heading, Text, Card } from "@chakra-ui/react";
import { Link } from "react-router-dom";

function HomepageContent() {
  return (
    <>
      <Box
        backgroundImage="./AvisCarWithPhone.jpg"
        backgroundColor="transparent"
        backgroundRepeat="no-repeat"
        backgroundSize="cover"
        objectFit="cover"
        height="100vh"
        my="1"
      >
        <Heading
          fontSize="3rem"
          color="White"
          fontFamily="poppins"
          pt="6rem"
          pb="2rem"
          fontWeight="700"
        ></Heading>

        <Box
          height="100%"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Link to="/options">
            <Card
              width="150px"
              height="150px"
              borderRadius="75px"
              display="flex"
              justifyContent="center"
              alignItems="center"
              backgroundColor="#eb3c14"
            >
              <Text color="white" fontWeight="700" fontSize="1.2rem">
                Get Started
              </Text>
            </Card>
          </Link>
        </Box>
      </Box>
    </>
  );
}
export default HomepageContent;
