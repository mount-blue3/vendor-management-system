import React from "react";
import Header from "./Header";
import HomepageContent from "./HomePageContent";
function Home() {
  return (
    <>
      <Header />
      <HomepageContent />
    </>
  );
}
export default Home;
