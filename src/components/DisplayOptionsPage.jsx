import React from "react";
import { Link } from "react-router-dom";
import { Box, Heading, Card, Flex, Image } from "@chakra-ui/react";
import Header from "./Header";
function DisplayOptionsPage() {
  return (
    <>
      <Header />
      <Box
        backgroundImage="./AvisBudgetGroupMobile.jpg"
        backgroundColor="transparent"
        backgroundRepeat="no-repeat"
        backgroundSize="cover"
        height="100vh"
        mt="1"
        pl="30rem"
        pt="7rem"
      >
        <Box>
          <Flex columnGap="6rem">
            <Link to="/newVendor">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./addVendor.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Add A New Vendor
                  </Heading>
                </Box>
              </Card>
            </Link>
            <Link to="/allVendors">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./showVendors.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Display all Vendors
                  </Heading>
                </Box>
              </Card>
            </Link>
          </Flex>
        </Box>
        <Box mt="4rem">
          <Flex columnGap="6rem">
            <Link to="/newResource">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./addResources.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Add A New Resource
                  </Heading>
                </Box>
              </Card>
            </Link>
            <Link to="/allResources">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./resources.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Display All Resources
                  </Heading>
                </Box>
              </Card>
            </Link>
          </Flex>
        </Box>
        <Box mt="4rem">
          <Flex columnGap="6rem">
            <Link to="/newProject">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./addResources.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Add A New Project
                  </Heading>
                </Box>
              </Card>
            </Link>
            <Link to="/allProjects">
              <Card
                width="20rem"
                height="10rem"
                backgroundColor="#eb3c14"
                boxShadow="2px 2px 4px 2px rgb(0,0,0,0.2)"
              >
                <Box
                  display="flex"
                  flexDirection="column"
                  justifyContent="center"
                  alignItems="center"
                  height="100%"
                >
                  <Image
                    src="./resources.png"
                    alt="Vendor Image"
                    boxSize="75px"
                  />
                  <Heading fontSize="1rem" mt="1rem" color="white">
                    Display All Projects
                  </Heading>
                </Box>
              </Card>
            </Link>
          </Flex>
        </Box>
      </Box>
    </>
  );
}
export default DisplayOptionsPage;
