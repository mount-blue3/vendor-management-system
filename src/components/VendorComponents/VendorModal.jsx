import React from "react";
import {
  Box,
  Text,
  Flex,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
} from "@chakra-ui/react";

import { EditIcon } from "@chakra-ui/icons";
import { useForm } from "react-hook-form";

import { useDispatch, useSelector } from "react-redux";
import { updateVendor, setIsEditing } from "../../store/slice/vendorSlice";
// import { useNavigate } from "react-router-dom";

const VendorModal = (props) => {
  let vendor = props.vendor;
  const reduxDispatch = useDispatch();
  let isEditing = useSelector((state) => {
    return state.vendor.isEditing;
  });
  // const navigate = useNavigate();

  const {
    register,
    setValue,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      vendorName: vendor.name,
      shortName: vendor.short_name,
      contactPersonName: vendor.person_of_contact_name,
      contactPersonEmail: vendor.person_of_contact_email,
      contactPersonMobileNumber: vendor.person_of_contact_mobile,
    },
  });
  const vendorName = watch("vendorName");
  const { ref } = register("vendorName");

  const handleOnBlur = () => {
    console.log("calling on blur");
    const generateShortName = () => {
      let shortName = [];
      vendorName
        .split(" ")
        .map((item) => shortName.push(item[0]?.toUpperCase()));
      setValue("shortName", shortName.join(""));
    };
    if (vendorName) {
      generateShortName();
    }
  };

  const handleEditClick = (e) => {
    reduxDispatch(setIsEditing(true));

    setValue("vendorName", vendor.name);
    setValue("shortName", vendor.short_name);
    setValue("contactPersonName", vendor.person_of_contact_name);
    setValue("contactPersonEmail", vendor.person_of_contact_email);
    setValue("contactPersonMobileNumber", vendor.person_of_contact_mobile);
  };

  const onSubmit = async (data) => {
    let updatedVendorObject = {
      id: vendor._id,
      name: data.vendorName,
      person_of_contact_name: data.contactPersonName,
      person_of_contact_email: data.contactPersonEmail,
      person_of_contact_mobile: data.contactPersonMobileNumber,
      short_name: data.shortName,
    };
    reduxDispatch(updateVendor(updatedVendorObject));
  };

  const handleCancelChanges = () => {
    reduxDispatch(setIsEditing(false));
  };

  const handleClose = () => {
    reduxDispatch(setIsEditing(false));
    props.close();
    if (!props.blockHistoryBack) history.back();
  };

  return (
    <>
      <Modal isOpen={props.open} onClose={handleClose} size="xl">
        <ModalOverlay />

        <ModalContent>
          <ModalHeader>Vendor Information</ModalHeader>
          <ModalCloseButton />

          <ModalBody>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Flex flexDirection="column" rowGap="1rem">
                <Text fontWeight="bold">
                  Vendor Name <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("vendorName", {
                    required: "This field is required",
                  })}
                  readOnly={!isEditing}
                  onBlur={handleOnBlur}
                  ref={ref}
                ></Input>
                <Text mt={2} color="red">
                  {errors.vendorName?.message}
                </Text>
                <Text fontWeight="bold">
                  Short Name <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("shortName", {
                    required: "This field is required",
                  })}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.shortName?.message}
                </Text>

                <Text fontWeight="bold">
                  Contact Person Name <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("contactPersonName", {
                    required: "This field is required",
                  })}
                  {...register("contactPersonName")}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.contactPersonName?.message}
                </Text>

                <Text fontWeight="bold">
                  Contact Person Email <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("contactPersonEmail", {
                    required: "This field is required",
                  })}
                  {...register("contactPersonEmail")}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.contactPersonEmail?.message}
                </Text>

                <Text fontWeight="bold">
                  Contact Person Number <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("contactPersonMobileNumber", {
                    required: "This field is required",
                  })}
                  {...register("contactPersonMobileNumber")}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.contactPersonMobileNumber?.message}
                </Text>

                <Box display="flex" justifyContent="end">
                  {isEditing && (
                    <>
                      <Button colorScheme="blue" mr={3} type="submit">
                        Save Changes
                      </Button>
                      <Button
                        colorScheme="blue"
                        mr={3}
                        onClick={handleCancelChanges}
                      >
                        Cancel
                      </Button>
                    </>
                  )}
                </Box>
              </Flex>
            </form>
            {!isEditing && (
              <Box>
                <Flex alignItems="end">
                  <Button
                    colorScheme="blue"
                    size="lg"
                    leftIcon={<EditIcon />}
                    mr={3}
                    onClick={handleEditClick}
                  >
                    Edit
                  </Button>
                </Flex>
              </Box>
            )}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default VendorModal;
