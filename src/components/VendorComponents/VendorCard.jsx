import React from "react";
import { Card, Heading, Flex, useDisclosure, Button } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import VendorModal from "./VendorModal";
import DeleteConfirmation from "../DeleteConfirmation";

const VendorCard = (props) => {
  let vendor = props.vendorData;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isDialogOpen,
    onOpen: onDialogOpen,
    onClose: onDialogClose,
  } = useDisclosure();

  const handleClick = () => {
    const newUrl = `/allVendors/${vendor.name.replace(/\s+/g, "-")}`;
    window.history.pushState(null, null, newUrl);
    onOpen();
  };

  const handleDelete = (e) => {
    props.delete(e, vendor._id);
  };

  return (
    <>
      <Card
        bgColor="#FA9884"
        color="white"
        width="20rem"
        height="4rem"
        boxShadow="md"
        cursor={"pointer"}
        onClick={() => handleClick()}
        m="2.5rem"
        p="2"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Heading size="md">{vendor.name}</Heading>
          <Button
            bgColor="#FA9884"
            onClick={(e) => {
              e.stopPropagation();
              onDialogOpen();
            }}
          >
            <DeleteIcon color="red" />
          </Button>
        </Flex>
      </Card>
      <DeleteConfirmation
        handleDelete={handleDelete}
        category="Vendor"
        isOpen={isDialogOpen}
        onClose={onDialogClose}
      />
      {isOpen && <VendorModal open={isOpen} close={onClose} vendor={vendor} />}
    </>
  );
};

export default VendorCard;
