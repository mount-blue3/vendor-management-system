import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import {
  FormControl,
  FormLabel,
  Button,
  Input,
  Text,
  Card,
  Heading,
  Box,
} from "@chakra-ui/react";
import Header from "../Header";
import { ArrowBackIcon } from "@chakra-ui/icons";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addVendor } from "../../store/slice/vendorSlice";

function VendorRegistrationForm() {
  const reduxDispatch = useDispatch();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      vendorName: "",
      shortName: "",
      contactPersonName: "",
      contactPersonEmail: "",
      contactPersonMobileNumber: "",
    },
  });
  const vendorName = watch("vendorName");
  const { ref } = register("vendorName");

  const handleOnBlur = () => {
    const generateShortName = () => {
      let shortName = [];
      vendorName
        .split(" ")
        .map((item) => shortName.push(item[0]?.toUpperCase()));
      setValue("shortName", shortName.join(""));
    };
    if (vendorName) {
      generateShortName();
    }
  };

  const onSubmit = async (data) => {
    let newVendorObject = {
      name: data.vendorName,
      person_of_contact_name: data.contactPersonName,
      person_of_contact_email: data.contactPersonEmail,
      person_of_contact_mobile: data.contactPersonMobileNumber,
      short_name: data.shortName,
    };
    reduxDispatch(addVendor(newVendorObject));
    reset();
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box
        width="100%"
        height="100vh"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Heading color="black" my="5">
          Create Vendor
        </Heading>
        <Card width="50%" boxShadow="2px 2px 4px 2px rgba(0, 0, 0, 0.2)">
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormControl padding="2rem">
              <FormLabel>
                Name of the vendor <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("vendorName", {
                  required: "This field is required",
                })}
                onBlur={handleOnBlur}
                ref={ref}
              />
              <Text mt={2} color="red">
                {errors.vendorName?.message}
              </Text>
              <FormLabel>
                Short Name <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("shortName", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.shortName?.message}
              </Text>
              <FormLabel>
                Contact Person Name <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("contactPersonName", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.contactPersonName?.message}
              </Text>
              <FormLabel>
                Contact Person Email <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="email"
                {...register("contactPersonEmail", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.contactPersonEmail?.message}
              </Text>
              <FormLabel>
                Contact Person Mobile Number{" "}
                <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="mobile"
                {...register("contactPersonMobileNumber", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors?.contactPersonMobileNumber?.message}
              </Text>
            </FormControl>

            <Box display="flex" justifyContent="center">
              <Button
                type="submit"
                width="20rem"
                color="white"
                mb="2rem"
                bgColor="blue"
              >
                Submit
              </Button>
            </Box>
          </form>
        </Card>
      </Box>
    </>
  );
}

export default VendorRegistrationForm;
