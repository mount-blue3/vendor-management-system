import React, { useEffect } from "react";
import { Box, Heading, Flex, Button } from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";
import Header from "../Header";
import { Link } from "react-router-dom";
import VendorCard from "./VendorCard";
import { useDispatch } from "react-redux";
import { removeVendor, vendorsData } from "../../store/slice/vendorSlice";
import { useSelector } from "react-redux";

function VendorsData() {
  const reduxDispatch = useDispatch();
  let vendors = useSelector((state) => {
    return state.vendor.vendors;
  });

  useEffect(() => {
    reduxDispatch(vendorsData());
  }, []);

  const handleDeleteVendor = async (event, id) => {
    event.stopPropagation();
    reduxDispatch(removeVendor(id));
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box height="auto">
        <Flex flexDirection="column" alignItems="center">
          <Heading color="black">All Vendors</Heading>
          <Flex flexWrap="wrap" justifyContent="center">
            {vendors?.map((item) => (
              <VendorCard
                key={item._id}
                vendorData={item}
                delete={handleDeleteVendor}
              />
            ))}
          </Flex>
        </Flex>
      </Box>
    </>
  );
}

export default VendorsData;
