import React from "react";
import { Card, Heading, Flex, useDisclosure, Button } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import ResourceModal from "./ResourceModal";
import DeleteConfirmation from "../DeleteConfirmation";

const ResourceCard = (props) => {
  let resource = props.resourceData;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isDialogOpen,
    onOpen: onDialogOpen,
    onClose: onDialogClose,
  } = useDisclosure();

  const handleClick = () => {
    onOpen();
  };

  const handleDelete = (e) => {
    props.delete(e, resource._id);
  };

  return (
    <>
      <Card
        bgColor="#FA9884"
        color="white"
        width="20rem"
        height="4rem"
        boxShadow="md"
        cursor={"pointer"}
        onClick={() => handleClick()}
        m="2.5rem"
        p="2"
      >
        <Flex justifyContent="space-between" alignItems="center">
          <Heading size="md">
            {resource.first_name + " " + resource.last_name}
          </Heading>
          <Button
            bgColor="#FA9884"
            onClick={(e) => {
              e.stopPropagation();
              onDialogOpen();
            }}
          >
            <DeleteIcon color="red" />
          </Button>
        </Flex>
      </Card>
      <DeleteConfirmation
        handleDelete={handleDelete}
        category="Resource"
        isOpen={isDialogOpen}
        onClose={onDialogClose}
      />
      {isOpen && (
        <ResourceModal open={isOpen} close={onClose} resource={resource} />
      )}
    </>
  );
};

export default ResourceCard;
