import React, { useEffect } from "react";
import { Box, Heading, Flex, Button } from "@chakra-ui/react";
import { ArrowBackIcon } from "@chakra-ui/icons";
import Header from "../Header";
import { Link } from "react-router-dom";
import ResourceCard from "./ResourceCard";
import { resourcesData, removeResource } from "../../store/slice/resourceSlice";
import { useDispatch, useSelector } from "react-redux";

const ResourcesData = () => {
  const reduxDispatch = useDispatch();
  let resources = useSelector((state) => {
    return state.resource.resources;
  });
  useEffect(() => {
    reduxDispatch(resourcesData());
  }, []);

  const handleDeleteResource = async (event, id) => {
    event.stopPropagation();
    reduxDispatch(removeResource(id));
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box height="auto">
        <Flex flexDirection="column" alignItems="center">
          <Heading color="black">All Resources</Heading>
          <Flex flexWrap="wrap" justifyContent="center">
            {resources?.map((item) => (
              <ResourceCard
                key={item._id}
                resourceData={item}
                delete={handleDeleteResource}
              />
            ))}
          </Flex>
        </Flex>
      </Box>
    </>
  );
};

export default ResourcesData;
