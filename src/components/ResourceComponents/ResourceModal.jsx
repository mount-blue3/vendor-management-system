import React, { useEffect } from "react";
import {
  Box,
  Text,
  Flex,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
} from "@chakra-ui/react";
import { EditIcon, ChevronDownIcon } from "@chakra-ui/icons";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { vendorsData } from "../../store/slice/vendorSlice";
import { updateResource, setIsEditing } from "../../store/slice/resourceSlice";

const ResourceModal = (props) => {
  const resource = props.resource;
  const reduxDispatch = useDispatch();
  let isEditing = useSelector((state) => {
    return state.resource.isEditing;
  });
  let vendors = useSelector((state) => {
    return state.vendor.vendors;
  });

  const {
    register,
    setValue,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      firstName: resource.first_name,
      lastName: resource.last_name,
      vendor: resource.vendor_resource_id,
      email: resource.email,
      mobile: resource.phone_number,
    },
  });
  const vendor = watch("vendor");
  useEffect(() => {
    reduxDispatch(vendorsData());
  }, []);

  const handleEditClick = () => {
    reduxDispatch(setIsEditing(true));
    setValue("firstName", resource.first_name);
    setValue("lastName", resource.last_name);
    setValue("vendor", resource.vendor_resource_id);
    setValue("email", resource.email);
    setValue("mobile", resource.phone_number);
  };

  const onSubmit = async (data) => {
    let updatedResourceObject = {
      id: resource._id,
      first_name: data.firstName,
      last_name: data.lastName,
      vendor_resource_id: data.vendor,
      email: data.email,
      phone_number: data.mobile,
    };

    reduxDispatch(updateResource(updatedResourceObject));
  };

  const handleCancelChanges = () => {
    reduxDispatch(setIsEditing(false));
  };

  const handleClose = () => {
    reduxDispatch(setIsEditing(false));
    props.close();
  };

  return (
    <>
      <Modal isOpen={props.open} onClose={handleClose} size="xl">
        <ModalOverlay />

        <ModalContent>
          <ModalHeader>Resource Information</ModalHeader>
          <ModalCloseButton />

          <ModalBody>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Flex flexDirection="column" rowGap="1rem">
                <Text fontWeight="bold">
                  First Name: <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  type="text"
                  {...register("firstName", { required: "This is required" })}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.firstName?.message}
                </Text>

                <Text fontWeight="bold">
                  Last Name: <span style={{ color: "red" }}>*</span>
                </Text>
                <Input
                  {...register("lastName", { required: "This is required" })}
                  readOnly={!isEditing}
                ></Input>
                <Text mt={2} color="red">
                  {errors.lastName?.message}
                </Text>

                <Text fontWeight="bold">
                  Vendors: <span style={{ color: "red" }}>*</span>
                </Text>
                <Menu {...register("vendor", { required: "This is required" })}>
                  <MenuButton
                    as={Button}
                    isDisabled={!isEditing}
                    rightIcon={<ChevronDownIcon />}
                  >
                    {vendor[1] ? vendor[1] : "Vendors"}
                  </MenuButton>
                  <MenuList>
                    <MenuItem onClick={() => setValue("vendor", [])}>
                      Vendors
                    </MenuItem>
                    {vendors?.map((item, index) => (
                      <MenuItem
                        key={index}
                        onClick={() =>
                          setValue("vendor", [item._id, item.name])
                        }
                      >
                        {item?.name}
                      </MenuItem>
                    ))}
                  </MenuList>
                </Menu>
                <Text fontWeight="bold">Email:</Text>
                <Input
                  {...register("email", {
                    pattern: {
                      value:
                        /^(?!\.)[\w.-]*[a-zA-Z0-9]@(?!-)[a-zA-Z0-9.-]+(?<!\.)\.[a-zA-Z]{2,63}$/,
                      message: "Invalid email format",
                    },
                  })}
                  readOnly={!isEditing}
                />
                <Text color="red">{errors.email?.message}</Text>
                <Text fontWeight="bold">Mobile:</Text>
                <Input
                  type="number"
                  {...register("mobileNumber", {
                    minLength: {
                      value: 10,
                      message: "Contact number must be at least 10 digits long",
                    },
                  })}
                  readOnly={!isEditing}
                />
                <Text color="red">{errors.mobileNumber?.message}</Text>
                <Box display="flex" justifyContent="end">
                  {isEditing && (
                    <>
                      <Button colorScheme="blue" mr={3} type="submit">
                        Save Changes
                      </Button>
                      <Button
                        colorScheme="blue"
                        mr={3}
                        onClick={handleCancelChanges}
                      >
                        Cancel
                      </Button>
                    </>
                  )}
                </Box>
              </Flex>
            </form>
            {!isEditing && (
              <Box>
                <Button
                  colorScheme="blue"
                  size="lg"
                  leftIcon={<EditIcon />}
                  mr={3}
                  onClick={handleEditClick}
                >
                  Edit
                </Button>
              </Box>
            )}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ResourceModal;
