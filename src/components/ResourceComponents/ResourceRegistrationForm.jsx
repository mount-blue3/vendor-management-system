import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import {
  FormControl,
  FormLabel,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Button,
  Input,
  Text,
  Card,
  Heading,
} from "@chakra-ui/react";
import { ArrowBackIcon, ChevronDownIcon } from "@chakra-ui/icons";
import { Box } from "@chakra-ui/react";
import Header from "../Header";
import { Link } from "react-router-dom";
import { addResource } from "../../store/slice/resourceSlice";
import { useSelector, useDispatch } from "react-redux";
import { vendorsData } from "../../store/slice/vendorSlice";

function ResourceRegistrationForm() {
  const [selectedVendor, setSelectedVendor] = useState("");
  const reduxDispatch = useDispatch();
  let vendors = useSelector((state) => {
    return state.vendor.vendors;
  });
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
    reset,
  } = useForm({
    defaultValues: {
      firstName: "",
      lastName: "",
      vendor: [],
      email: "",
      mobileNumber: "",
    },
  });

  useEffect(() => {
    reduxDispatch(vendorsData());
  }, []);

  useEffect(() => {
    if (selectedVendor !== "") {
      vendors.find((item) => {
        if (item.name === selectedVendor) {
          setValue("vendor", [item._id, item.name]);
        }
      });
    }
  }, [selectedVendor, setValue]);
  const onSubmit = async (data) => {
    const newResourceObject = {
      first_name: data.firstName,
      last_name: data.lastName,
      email: data.email,
      vendor_resource_id: data.vendor,
      phone_number: data.mobileNumber,
    };
    reduxDispatch(addResource(newResourceObject));
    setSelectedVendor("");
    reset();
  };

  return (
    <>
      <Header />
      <Box p="3">
        <Link to="/options">
          <Button
            bg="blue"
            leftIcon={<ArrowBackIcon />}
            color="white"
            size="lg"
            my="5"
          >
            Back
          </Button>
        </Link>
      </Box>
      <Box
        width="100%"
        height="100vh"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <Heading color="Black" my="5">
          Create Resource
        </Heading>
        <Card width="50%" boxShadow="2px 2px 4px 2px rgba(0, 0, 0, 0.2)">
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormControl padding="2rem">
              <FormLabel>
                First Name<span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("firstName", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.firstName?.message}
              </Text>
              <FormLabel mt="1rem">
                Last Name <span style={{ color: "red" }}>*</span>
              </FormLabel>
              <Input
                type="text"
                {...register("lastName", {
                  required: "This field is required",
                })}
              />
              <Text mt={2} color="red">
                {errors.lastName?.message}
              </Text>
              <Text mb="1rem" mt="1rem">
                Select vendor<span style={{ color: "red" }}>*</span>
              </Text>
              <Menu {...register("vendor", { required: "This is required" })}>
                <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
                  {selectedVendor ? selectedVendor : "Vendors"}
                </MenuButton>
                <MenuList>
                  <MenuItem onClick={() => setSelectedVendor("")}>
                    Vendors
                  </MenuItem>
                  {vendors?.map((item, index) => (
                    <MenuItem
                      key={index}
                      onClick={() => setSelectedVendor(item.name)}
                    >
                      {item?.name}
                    </MenuItem>
                  ))}
                </MenuList>
              </Menu>
              <FormLabel mt="1rem">Email</FormLabel>
              <Input
                {...register("email", {
                  pattern: {
                    value:
                      /^(?!\.)[\w.-]*[a-zA-Z0-9]@(?!-)[a-zA-Z0-9.-]+(?<!\.)\.[a-zA-Z]{2,63}$/,
                    message: "Invalid email format",
                  },
                })}
              />
              <Text mt={2} color="red">
                {errors.email?.message}
              </Text>
              <FormLabel mt="1rem">Contact Number</FormLabel>
              <Input
                type="number"
                {...register("mobileNumber", {
                  minLength: {
                    value: 10,
                    message:
                      "Contact number must be at least 10 digits long",
                  },
                })}
              />
              <Text mt={2} color="red">
                {errors.mobileNumber?.message}
              </Text>
            </FormControl>
            <Box display="flex" justifyContent="center">
              <Button
                type="submit"
                width="20rem"
                bgColor="blue"
                mb="2rem"
                color="white"
              >
                Submit
              </Button>
            </Box>
          </form>
        </Card>
      </Box>
    </>
  );
}

export default ResourceRegistrationForm;
